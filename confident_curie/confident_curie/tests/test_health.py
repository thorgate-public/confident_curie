from django.conf import settings
from django.core.cache import cache
from django.test import Client
from django.utils.datetime_safe import datetime

import pytest

from pytest_mock import MockerFixture
from tg_utils.health_check.checks.celery_beat.backends import CACHE_KEY, TIMEOUT


@pytest.fixture(autouse=True)
def patch_celery(mocker: MockerFixture):
    cache.set(CACHE_KEY, datetime.now(), timeout=TIMEOUT * 2)
    mocker.patch("health_check.contrib.celery.backends.CeleryHealthCheck.check_status")
    yield


@pytest.mark.django_db
def test_health_url(django_client: Client):
    response = django_client.get("/_health")
    assert response.status_code == 200
    assert response.json() == {"error": False}


@pytest.mark.django_db
def test_health_detail_no_token(django_client: Client):
    response = django_client.get("/_health/details")
    assert response.status_code == 403

    response = django_client.get("/_health/details?healthtoken=fake")
    assert response.status_code == 403


@pytest.mark.django_db
def test_health_detail(django_client: Client):
    response = django_client.get(
        "/_health/details?healthtoken={healthtoken}".format(
            healthtoken=settings.HEALTH_CHECK_ACCESS_TOKEN,
        )
    )
    assert response.status_code == 200
