from confident_curie.celery import app as celery_app


default_app_config = "confident_curie.apps.ConfidentCurieConfig"


__all__ = ["celery_app"]
