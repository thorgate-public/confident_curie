import { baseQueriesApi as api } from "./baseQueriesApi";
export const addTagTypes = ["auth", "user"] as const;
const injectedRtkApi = api
    .enhanceEndpoints({
        addTagTypes,
    })
    .injectEndpoints({
        endpoints: (build) => ({
            authTokenCreate: build.mutation<
                AuthTokenCreateApiResponse,
                AuthTokenCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/auth/token/`,
                    method: "POST",
                    body: queryArg.tokenObtainPair,
                }),
                invalidatesTags: ["auth"],
            }),
            authTokenRefreshCreate: build.mutation<
                AuthTokenRefreshCreateApiResponse,
                AuthTokenRefreshCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/auth/token/refresh/`,
                    method: "POST",
                    body: queryArg.tokenRefresh,
                }),
                invalidatesTags: ["auth"],
            }),
            authTokenVerifyCreate: build.mutation<
                AuthTokenVerifyCreateApiResponse,
                AuthTokenVerifyCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/auth/token/verify/`,
                    method: "POST",
                    body: queryArg.tokenVerify,
                }),
                invalidatesTags: ["auth"],
            }),
            userForgotPasswordCreate: build.mutation<
                UserForgotPasswordCreateApiResponse,
                UserForgotPasswordCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/user/forgot_password`,
                    method: "POST",
                    body: queryArg.forgotPassword,
                }),
                invalidatesTags: ["user"],
            }),
            userForgotPasswordTokenCreate: build.mutation<
                UserForgotPasswordTokenCreateApiResponse,
                UserForgotPasswordTokenCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/user/forgot_password/token`,
                    method: "POST",
                    body: queryArg.recoveryPassword,
                }),
                invalidatesTags: ["user"],
            }),
            userMeRetrieve: build.query<
                UserMeRetrieveApiResponse,
                UserMeRetrieveApiArg
            >({
                query: () => ({ url: `/api/user/me` }),
                providesTags: ["user"],
            }),
            userMeUpdate: build.mutation<
                UserMeUpdateApiResponse,
                UserMeUpdateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/user/me`,
                    method: "PUT",
                    body: queryArg.userDetails,
                }),
                invalidatesTags: ["user"],
            }),
            userMePartialUpdate: build.mutation<
                UserMePartialUpdateApiResponse,
                UserMePartialUpdateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/user/me`,
                    method: "PATCH",
                    body: queryArg.patchedUserDetails,
                }),
                invalidatesTags: ["user"],
            }),
            userSignupCreate: build.mutation<
                UserSignupCreateApiResponse,
                UserSignupCreateApiArg
            >({
                query: (queryArg) => ({
                    url: `/api/user/signup`,
                    method: "POST",
                    body: queryArg.signup,
                }),
                invalidatesTags: ["user"],
            }),
        }),
        overrideExisting: false,
    });
export { injectedRtkApi as queriesApi };
export type AuthTokenCreateApiResponse = /** status 200  */ TokenObtainPair;
export type AuthTokenCreateApiArg = {
    tokenObtainPair: TokenObtainPair;
};
export type AuthTokenRefreshCreateApiResponse = /** status 200  */ TokenRefresh;
export type AuthTokenRefreshCreateApiArg = {
    tokenRefresh: TokenRefresh;
};
export type AuthTokenVerifyCreateApiResponse = /** status 200  */ TokenVerify;
export type AuthTokenVerifyCreateApiArg = {
    tokenVerify: TokenVerify;
};
export type UserForgotPasswordCreateApiResponse =
    /** status 200  */ ForgotPassword;
export type UserForgotPasswordCreateApiArg = {
    forgotPassword: ForgotPassword;
};
export type UserForgotPasswordTokenCreateApiResponse =
    /** status 200  */ RecoveryPassword;
export type UserForgotPasswordTokenCreateApiArg = {
    recoveryPassword: RecoveryPassword;
};
export type UserMeRetrieveApiResponse = /** status 200  */ UserDetails;
export type UserMeRetrieveApiArg = void;
export type UserMeUpdateApiResponse = /** status 200  */ UserDetails;
export type UserMeUpdateApiArg = {
    userDetails: UserDetails;
};
export type UserMePartialUpdateApiResponse = /** status 200  */ UserDetails;
export type UserMePartialUpdateApiArg = {
    patchedUserDetails: PatchedUserDetails;
};
export type UserSignupCreateApiResponse = /** status 200  */ Signup;
export type UserSignupCreateApiArg = {
    signup: Signup;
};
export type TokenObtainPair = {
    email: string;
    password: string;
    access: string;
    refresh: string;
};
export type TokenRefresh = {
    access: string;
    refresh: string;
};
export type TokenVerify = {
    token: string;
};
export type ForgotPassword = {
    email: string;
};
export type RecoveryPassword = {
    password: string;
    passwordConfirm: string;
    uidAndTokenB64: string;
};
export type UserDetails = {
    id: number;
    password: string;
    lastLogin: string | null;
    isSuperuser: boolean;
    email: string;
    name: string;
    isStaff: boolean;
    isActive: boolean;
    dateJoined: string;
};
export type PatchedUserDetails = {
    id?: number;
    password?: string;
    lastLogin?: string | null;
    isSuperuser?: boolean;
    email?: string;
    name?: string;
    isStaff?: boolean;
    isActive?: boolean;
    dateJoined?: string;
};
export type Signup = {
    email: string;
    password: string;
    name: string;
};
export const {
    useAuthTokenCreateMutation,
    useAuthTokenRefreshCreateMutation,
    useAuthTokenVerifyCreateMutation,
    useUserForgotPasswordCreateMutation,
    useUserForgotPasswordTokenCreateMutation,
    useUserMeRetrieveQuery,
    useUserMeUpdateMutation,
    useUserMePartialUpdateMutation,
    useUserSignupCreateMutation,
} = injectedRtkApi;
