# Based on Node 16 Alpine image
FROM node:18-alpine

# Set the default directory where CMD will execute
WORKDIR /app

# Create a directory for the logs
RUN mkdir -p /var/log/confident_curie

# Mark assets directory as volume
VOLUME /files/assets

ENV NEXT_SHARP_PATH=/usr/local/lib/node_modules/sharp

# Copy package files
COPY ./app/package.json ./
COPY ./app/pnpm-lock.yaml ./

# Install node build dependencies
RUN apk add --no-cache libc6-compat bash grep && \
    apk add --no-cache --virtual .build-deps libc6-compat alpine-sdk python3 && \
    npm -g i pnpm sharp && \
    pnpm config set store-dir /usr/local/share/.cache/.pnpm-store && \
    pnpm install --frozen-lockfile && \
    apk del .build-deps

# Copy code
COPY ./app /app

# Define the PORT environment var so that razzle can pick it up in the following `pnpm build` command.
# It may be overridden in the .env file when the container is run.
ENV PORT 80

# Build node app
RUN pnpm build

# Set the default command to execute when creating a new container
CMD pnpm start -p 80
